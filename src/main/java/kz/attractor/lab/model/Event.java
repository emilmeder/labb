package kz.attractor.lab.model;
import kz.attractor.lab.util.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document
public class Event {
    private String id = UUID.randomUUID().toString();
    private String name;
    private String describe;
    private LocalDate date;
    private static Random rnd = new Random();

    public static Event makeNew() {
        int t = rnd.nextInt(2);
        LocalDate d = LocalDate.now();
        d = (t == 1) ? d.plusDays(rnd.nextInt(10)) : d.minusDays(rnd.nextInt(10));
        Event event = new Event();
        event.setDate(d);
        event.setName(Generator.makeName());
//        event.setMail(Generator.makeEmail());
        event.setDescribe(Generator.makeDescription());
        return event;
    }
}
