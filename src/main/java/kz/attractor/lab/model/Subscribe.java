package kz.attractor.lab.model;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document
public class Subscribe {
    private String id = UUID.randomUUID().toString();
    private Event eventId;
    private String mail;
    private LocalDate date;

    public static Subscribe makeNew(Event event, String mail, LocalDate date) {
        Subscribe sub = new Subscribe();
        sub.setEventId(event);
        sub.setMail(mail);
        sub.setDate(date);
        return sub;
    }


}
