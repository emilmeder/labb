package kz.attractor.lab.util;
import kz.attractor.lab.model.*;
import kz.attractor.lab.repository.EventRepository;
import kz.attractor.lab.repository.SubscribeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

    @Configuration
    public class DataBasePreloader {


//    private List<Event> makeEvent() {
//        List<Event> event = new ArrayList<>();
//        event.add(new Event("1", LocalDate.now()));
//        event.add(new Event("2",  LocalDate.now()));
//        event.add(new Event("3", LocalDate.now()));
//        return event;
//    }
    @Bean
    CommandLineRunner generateGibberish(EventRepository er) {
        return args -> {
            er.deleteAll();
            var events = Stream.generate(Event::makeNew).limit(5).collect(toList());
            er.saveAll(events);
            er.findAll().forEach(event -> System.out.println(event));
        };
    }

//    @Bean
//    CommandLineRunner initDatabase() {
//        er.deleteAll();
//        sr.deleteAll();
//
//
////        er.saveAll(makeEvent());
//
////        cr.save(new Comment("text1", LocalDateTime.now(), ur.findUserByName("rua").getId()));
////        cr.save(new Comment("text2", LocalDateTime.now(), ur.findUserByName("tj").getId()));
////        cr.save(new Comment("text3", LocalDateTime.now(), ur.findUserByName("cody").getId()));
////
////        er.save(user);
//
//        return null;
//    }

}
