package kz.attractor.lab.controller;

import kz.attractor.lab.Annatations.ApiPageable;
import kz.attractor.lab.dto.EventDTO;
import kz.attractor.lab.dto.SubscribeDTO;
import kz.attractor.lab.service.SubscribeService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/subscribes")
public class SubscribeController {

    private final SubscribeService ss;

    public SubscribeController(SubscribeService ss) {

        this.ss = ss;
    }

    @ApiPageable
    @GetMapping
    public Slice<SubscribeDTO> getAllSubscribes(@ApiIgnore Pageable pageable) {
        return ss.findAllSubscribes(pageable);
    }

    @ApiPageable
    @GetMapping("/{Mail}")
    public List<EventDTO> findAllUserEvents(
            @PathVariable("Mail") String Mail,
            @ApiIgnore Pageable pageable) {
        return ss.findAllUserEvents(Mail, pageable);
    }

    @DeleteMapping("/{Mail}")
    public String deleteSubscribe(
            @PathVariable("Mail") String Mail) {
        return ss.deleteSubscribe(Mail);
    }
}