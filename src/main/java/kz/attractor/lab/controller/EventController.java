package kz.attractor.lab.controller;

import kz.attractor.lab.Annatations.ApiPageable;
import kz.attractor.lab.dto.*;
import kz.attractor.lab.service.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/events")
public class EventController {
    public final EventService es;

    public EventController(EventService es) {

        this.es = es;
    }

    @ApiPageable
    @GetMapping
    public Slice<EventDTO> findAllEvents(@ApiIgnore Pageable pageable) {

        return es.findAllEvents(pageable);
    }

    @PutMapping("/{eventId}/{Mail}")
    public String subscribeToEvent(
        @PathVariable("eventId") String eventId,
        @PathVariable("Mail") String Mail) {
        return es.subscribeToEvent(eventId, Mail);
    }
}
