package kz.attractor.lab.repository;

import kz.attractor.lab.model.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubscribeRepository extends PagingAndSortingRepository<Subscribe, String> {
    Subscribe findByMailAndEventId(String mail, String eventId);
    Slice<Subscribe> findAllByMail(String mail, Pageable pageable);
    boolean existsByMail(String mail);
    void deleteByMail(String mail);
}
