package kz.attractor.lab.dto;


import kz.attractor.lab.model.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SubscribeDTO {

    public static SubscribeDTO from(Subscribe subscribe) {
        return builder()
                .id(subscribe.getId())
                .mail(subscribe.getMail())
                .eventId(subscribe.getEventId())
                .date(subscribe.getDate())
                .build();
    }
    private LocalDate date;
    private Event eventId;
    private String mail;
    private String id;

}
