package kz.attractor.lab.dto;


import kz.attractor.lab.model.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class EventDTO {

    public static EventDTO from(Event event) {
        return builder()
                .id(event.getId())
                .name(event.getName())
                .describe(event.getDescribe())
                .build();
    }
    private String id;
    private String name;
    private String describe;
    private LocalDate date;

}
