package kz.attractor.lab.service;
import kz.attractor.lab.model.*;
import kz.attractor.lab.dto.*;
import kz.attractor.lab.repository.*;
import kz.attractor.lab.service.*;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import kz.attractor.lab.repository.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {
    public final EventRepository er;
    public final SubscribeService ss;

    public EventService(EventRepository er, SubscribeService ss) {
        this.er = er;
        this.ss = ss;
    }

    public Slice<EventDTO> findAllEvents(Pageable pageable) {
        var slice = er.findAll(pageable);
        return slice.map(EventDTO::from);
    }

    public String subscribeToEvent(String eventId, String Mail) {
        String answer = "";
        Event event = er.findById(eventId).get();
        answer = ss.addSubscribe(event, Mail, LocalDate.now());
        return answer;
}




}
