package kz.attractor.lab.service;

import kz.attractor.lab.dto.*;
import kz.attractor.lab.model.*;
import kz.attractor.lab.repository.EventRepository;
import kz.attractor.lab.repository.SubscribeRepository;
import kz.attractor.lab.exception.ResourceNotFoundException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class SubscribeService {
    private final SubscribeRepository sr;

    public SubscribeService(SubscribeRepository sr) {

        this.sr = sr;
    }
    //    public SubscribeDTO addSubscribe(SubscribeDTO subscribeData) {
//        er.findById(subscribeData.getEventId());
//        var subscribe = Subscribe.builder()
//                .id(subscribeData.getId())
//                .eventId(subscribeData.getEventId())
//                .mail(subscribeData.getMail())
//                .build();
//
//        sr.save(subscribe);
//        return SubscribeDTO.from(subscribe);
//    }

    public String addSubscribe(Event event, String Mail, LocalDate date) {
            String answer = "";
            Subscribe s = sr.findByMailAndEventId(Mail, event.getId());
            answer = "You have already subscribed to: " + s.getEventId().getName() + " Subscribe ID: " + s.getId();
            s = Subscribe.makeNew(event, Mail, date);
            answer = "You are subscribed to: " + s.getEventId().getName() + " Subscribe ID: " + s.getId();
            sr.save(s);
            return answer;
    }
    public String deleteSubscribe(String Mail) {
        String answer = "";
        if(sr.existsByMail(Mail)) {
            sr.deleteByMail(Mail);
            answer = " didn't subscribed to " + Mail;
        }
        else {
            answer = " there is no matches";
        }
        return answer;
    }

    public Slice<SubscribeDTO> findAllSubscribes(Pageable pageable) {
        var slice = sr.findAll(pageable);
        return slice.map(SubscribeDTO::from);
    }

    public List<EventDTO> findAllUserEvents(String Mail, Pageable pageable) {
        var slice = sr.findAllByMail(Mail, pageable);
        List<EventDTO> userEvents = new ArrayList<>();
        slice.forEach(s -> userEvents.add(EventDTO.from(s.getEventId())));
        return userEvents;
    }


}


